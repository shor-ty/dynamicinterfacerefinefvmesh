# README #

This library is an extension to the dynamicRefineFvMesh and include an additional interface refinement. It should also be possible to use two scalars for refinement. 


### OpenFOAM Versions ###
* Build for Foundation OpenFOAM 2.3.x
* Build for Foundation OpenFOAM 5.x
* Probably work for ESI +v1706 (not tested and supported)


### How to get it work ###
* You can compile the lib where ever you want. This is just an example:
> mkdir -p $FOAM_RUN/../OpenFOAM_extensions
* Switch to this directory
> cd $FOAM_RUN/../OpenFOAM_extensions
* Clone the repository
> git clone https://bitbucket.org/shor-ty/dynamicinterfacerefinefvmesh.git
* Move to the new library folder
> cd dynamicinterfacerefinefvmesh
* Checkout the openfoam version you need (e.g. using 5.x)
> git checkout OpenFOAM-5.x
* Go to the library source
> cd src/dynamicFvMesh
* Compile the lib
> wmake libso
* Finally you have to include the libary into your solver set-up. Therefore add the following line to your dynamicMeshDict
> dynamicFvMeshLibs ( "libdynamicInterfaceRefineFvMesh.so" );
* Done


### Usage ###
* The best way is to copy the dynamicMeshDict to your case and modify it


### Contact ###
Tobias.Holzmann@Holzmann-cfd.de
